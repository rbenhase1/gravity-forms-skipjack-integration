<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across the plugin.
 *
 * @link       http://ryanbenhase.com
 * @since      1.0.0
 *
 * @package    Gravityforms_Skipjack
 * @subpackage Gravityforms_Skipjack/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization and hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Gravityforms_Skipjack
 * @subpackage Gravityforms_Skipjack/includes
 * @author     Ryan <rbenhase@2060digital.com>
 */
class Gravityforms_Skipjack {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Gravityforms_Skipjack_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'gravityforms-skipjack';
		$this->version = '1.0.2';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Gravityforms_Skipjack_Loader. Orchestrates the hooks of the plugin.
	 * - Gravityforms_Skipjack_i18n. Defines internationalization functionality.
	 * - Gravityforms_Skipjack_Payment. Defines all payment functionality/integration with Gravity Forms
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gravityforms-skipjack-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gravityforms-skipjack-i18n.php';
		
		/**
		 * The class responsible for integrating with the payment gateway
		 */
		if ( method_exists( 'GFForms', 'include_payment_addon_framework' ) ) {
      GFForms::include_payment_addon_framework();
      require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gravityforms-skipjack-payment.php';
      new Gravityforms_Skipjack_Payment();
    }

		$this->loader = new Gravityforms_Skipjack_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Gravityforms_Skipjack_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Gravityforms_Skipjack_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_hooks() {
		
		$this->loader->add_filter( 'gform_enable_credit_card_field', $this, 'enable_credit_card', 11 );
		$this->loader->add_action( 'admin_init', $this, 'check_gravityforms_active' ); 

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
  	$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Gravityforms_Skipjack_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
	
	
	/**
	 * Enables the Gravity Forms credit card field for all forms. 
	 * Relies on the 'gform_enable_credit_card_field' filter.
	 * 
	 * @access public
	 * @param Boolean $is_enabled
	 * @return true
	 */
	public function enable_credit_card( $is_enabled ) {
    return true;
  }
	
	
	public function check_gravityforms_active() {
  	if ( !is_plugin_active( 'gravityforms/gravityforms.php' ) ) {
    	$class = 'error';
      $message = 'Error: The Gravity Forms Skipjack Integration plugin requires <a href="http://www.gravityforms.com/" target="_blank">Gravity Forms</a> to be installed and active.';
      echo "<div class=\"$class\"> <p>$message</p></div>"; 
  	}
	}

}
