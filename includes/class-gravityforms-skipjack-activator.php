<?php

/**
 * Fired during plugin activation
 *
 * @link       http://ryanbenhase.com
 * @since      1.0.0
 *
 * @package    Gravityforms_Skipjack
 * @subpackage Gravityforms_Skipjack/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gravityforms_Skipjack
 * @subpackage Gravityforms_Skipjack/includes
 * @author     Ryan <rbenhase@2060digital.com>
 */
class Gravityforms_Skipjack_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
