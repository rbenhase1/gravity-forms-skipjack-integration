<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://ryanbenhase.com
 * @since      1.0.0
 *
 * @package    Gravityforms_Skipjack
 * @subpackage Gravityforms_Skipjack/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gravityforms_Skipjack
 * @subpackage Gravityforms_Skipjack/includes
 * @author     Ryan <rbenhase@2060digital.com>
 */
class Gravityforms_Skipjack_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
