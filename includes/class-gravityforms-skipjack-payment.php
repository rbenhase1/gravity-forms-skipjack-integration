<?php

/**
 * The file that defines the payment integration class for the plugin
 *
 * @link       http://ryanbenhase.com
 * @since      1.0.0
 *
 * @package    Gravityforms_Skipjack
 * @subpackage Gravityforms_Skipjack/includes
 */

/**
 * The payment integration class
 *
 * @since      1.0.0
 * @package    Gravityforms_Skipjack
 * @subpackage Gravityforms_Skipjack/includes
 * @author     Ryan <rbenhase@2060digital.com>
 */
class Gravityforms_Skipjack_Payment extends GFPaymentAddOn {
    protected $_version = "2.3.7"; 
    protected $_min_gravityforms_version = "1.8.12";
    protected $_slug = 'gravityformsskipjack';
    protected $_path = 'gravityforms-skipjack/gravityforms-skipjack.php';
    protected $_full_path = __FILE__;
    protected $_title = 'Gravity Forms SKIPJACK Integration Add-On';
    protected $_short_title = 'SKIPJACK';
    protected $_supports_callbacks = true;
    protected $_requires_credit_card = true;    
    
  
  /**
   * Add/modify fields on the Skipjack settings tab in Gravity Forms.
   * 
   * @access public
   * @return Array $default_settings The default settings to filter
   */
  public function feed_settings_fields() {
    $default_settings = parent::feed_settings_fields();
  
    $fields = array(
      array(
          'name'     => 'serial',
          'label'    => __( 'HTML Serial Number', 'gravityformsskipjack' ),
          'type'     => 'text',
          'class'    => 'small',
          'required' => true,
          'tooltip'  => '<h6>' . __( 'HTML Serial Number', 'gravityformsskipjack' ) . '</h6>' . 
                     __( 'Enter the 12-digit HTML serial number, provided by SKIPJACK, to use for transactions.', 'gravityformsskipjack' )
      ),
      array(
          'name'          => 'environment',
          'label'         => __( 'Environment', 'gravityformsskipjack' ),
          'type'          => 'radio',
          'choices'       => array(
                                array( 'id' => 'gf_paypal_mode_test', 'label' => __( 'Testing Only', 'gravityformsskipjack' ), 'value' => 'test' ),
                                array( 'id' => 'gf_skipjack_mode_production', 'label' => __( 'Live (Production)', 'gravityformsskipjack' ), 'value' => 'production' ),
                             ),
          'horizontal'    => true,
          'default_value' => 'test',
          'required'      => true,
          'tooltip'       => '<h6>' . __( 'Environment', 'gravityformsskipjack' ) . '</h6>' . 
                          __( 'Whether to simulate processing credit cards in the testing environment or to perform real transactions in the live environment.', 'gravityformsskipjack' )
      ),
    );
    
    $transaction_type = parent::get_field( 'transactionType', $default_settings );
    $choices          = $transaction_type['choices'];
    $add_donation     = true;
    foreach ( $choices as $k => $choice ) {
        
        // Remove subscription option, add donation option if it doesn't exist
        if ( $choice['value'] == 'subscription' ) {
            unset( $choices[$k] );
        } else if ( $choice['value'] == 'subscription' ) {
            $add_donation = false;
        }
    }
    if ( $add_donation ) {
        //Add donation transaction type
        $choices[] = array( 'label' => __( 'Donation', 'gravityformspaypal' ), 'value' => 'donation' );
    }
    $transaction_type['choices'] = $choices;
    $default_settings = $this->replace_field( 'transactionType', $transaction_type, $default_settings );

    
    $default_settings = parent::remove_field( 'options', $default_settings );  
    $default_settings = parent::add_field_after( 'feedName', $fields, $default_settings );
    return $default_settings;
  }
    
  /**
   * Authorize the data with Skipjack.
   * 
   * @access public
   * @param Array $feed The active payment feed and configuration data
   * @param Array $submission_data The form field data submitted by the user, including payment info
   * @param Array $form The form and all its settings
   * @param Array $entry The current entry array before it is saved to the database
   * @return Array $result An associative array containing the is_authorized, error_message, and transaction_id properties.
   *                       Also include a 'captured_payment' array with further information about the payment:
   *                             is_success, error_message, transaction_id, amount
   */
  public function authorize( $feed, $submission_data, $form, $entry ) {
    
    
    // Decrypt submission data for authorization
    $encrypted = array(
      'email' => $submission_data['email'],
      'address' => $submission_data['address'],
      'address2' => $submission_data['address2'],
      'city' => $submission_data['city'],
      'state' => $submission_data['state'],
      'zip' => $submission_data['zip'],
      'country' => $submission_data['country'],
      'payment_amount' => $submission_data['payment_amount']
    );
    
    // If Gravitate Encryption class exists, decrypt submission data before sending to SKIPJACK
    if( class_exists( 'MCrypt_Encryption' ) ) {
      
      foreach( $encrypted as $key => $value ) 
        $submission_data[$key] = MCrypt_Encryption::decrypt( $value );
    }

    // Endpoint depends on environment
    $endpoint = 'https://developer.skipjackic.com/scripts/EvolvCC.dll?AuthorizeAPI';
    if ( $feed['meta']['environment'] == 'production' ) {
      $endpoint = 'https://www.skipjackic.com/scripts/evolvcc.dll?AuthorizeAPI';
    }
    
    if ( empty( $submission_data['payment_amount'] ) ) {
      return array(
        'is_authorized' => false,
        'error_message' => 'Invalid payment amount: ' . $submission_data['payment_amount'],
        'transaction_id' => 0
      );
    }
        
    // Set up data to send in request
    $data = array(
      "SerialNumber" => $feed['meta']['serial'],
      "SJName" => str_replace( '&', '', $submission_data['card_name'] ),
      'Email' => $submission_data['email'],
      'StreetAddress' => $submission_data['address'],
      'StreetAddress2' => $submission_data['address2'],
      'City' => $submission_data['city'],
      'State' => $submission_data['state'],
      'ZipCode' => $submission_data['zip'],
      'AccountNumber' => $submission_data['card_number'],
      'Month' => $submission_data['card_expiration_date'][0],
      'Year' => $submission_data['card_expiration_date'][1],
      'CVV2' => $submission_data['card_security_code'],
      'TransactionAmount' => number_format( $submission_data['payment_amount'], 2, '.', '' ),
      'ShipToPhone' => '0000000000',
      'OrderString' => '1~None~0.00~0~N~||'
    );
    
    // Make our request using cURL.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $data ) );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    // Use included CA certificate
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_CAINFO, plugins_url( 'certs/skipjack.crt', dirname( __FILE__ ) ) );

    $response_string = curl_exec($ch);     
    curl_close ($ch);

    // Parse response
    $strings = explode( "\n", str_replace( array( "\r\n","\n\r","\r" ),"\n", $response_string ) );
    $response = array();      
    
    $keys = str_getcsv( $strings[0] );
    $values = str_getcsv( $strings[1] );
    
    foreach( $keys as $index => $key_name ) {
      $response[$key_name] = $values[$index];
    }
    
    // Do something with results                       
    if ( $response['szReturnCode'] != 1 ) {
      // Error in authorization/capture
      
      $result = array(
        'is_authorized' => false,
        'error_message' => 'There was an error while trying to process this transaction; the card was not charged. ' . 
                           'Please double check your information and try again. Error code: ' . $response['szReturnCode'],
        'transaction_id' => 0
      );
      
    } else if ( $response['szIsApproved'] == 0 ) {
      // Card Declined
      
      $result = array(
        'is_authorized' => false,
        'error_message' => 'Sorry, this transaction was declined. Please double check your information or try another card.',
        'transaction_id' => 0
      );
    } else {
      // Card Approved
      

      
      $result = array(
        'is_authorized' => true,
        'error_message' => ' **<pre>' . print_r( $submission_data, true ) . '</pre>**',
        'transaction_id' => $response['szTransactionFileName'],
        'captured_payment' => array(
          'is_success' => true,
          'error_message' => '',
          'transaction_id' => $response['szTransactionFileName'],
          'amount' => $data['TransactionAmount']
        )
      );
    }      
    return $result;      
  }    
  
  
  /**
   * If encryption is enabled, override the get_order_data() class method so that payment amount can be calculated.
   * Otherwise, it tries to add together multiple encrypted values, leading to broken transactions.
   * 
   * @access public
   * @param Array $feed The SKIPJACK feed data, configured in Gravity Forms
   * @param Array $form The form itself
   * @param Array $entry The form entry
   * @return Array Containing order data
   *
   * @since 1.0.1
   */
  public function get_order_data( $feed, $form, $entry ) {
    
    // If Gravitate Encryption is enabled, decrypt all prices before attempting to add
    if( class_exists( 'MCrypt_Encryption' ) ) {
      
  		$products = GFCommon::get_product_fields( $form, $entry );
  
  		$payment_field   = $feed['meta']['transactionType'] == 'product' ? rgars( $feed, 'meta/paymentAmount' ) : rgars( $feed, 'meta/recurringAmount' );
  		$setup_fee_field = rgar( $feed['meta'], 'setupFee_enabled' ) ? $feed['meta']['setupFee_product'] : false;
  		$trial_field     = rgar( $feed['meta'], 'trial_enabled' ) ? rgars( $feed, 'meta/trial_product' ) : false;
  
  		$amount       = 0;
  		$line_items   = array();
  		$discounts    = array();
  		$fee_amount   = 0;
  		$trial_amount = 0;
  		
  		foreach ( $products['products'] as $field_id => $product ) {						  			

			  $product_price = GFCommon::to_number( MCrypt_Encryption::decrypt( $product['price'] ) );
			  $quantity      = MCrypt_Encryption::decrypt( $product['quantity'] ) ? MCrypt_Encryption::decrypt( $product['quantity'] ): 1;

  			$options = array();
  			if ( is_array( rgar( $product, 'options' ) ) ) {
  				foreach ( $product['options'] as $option ) {    				
      		  $product_price += MCrypt_Encryption::decrypt( $option['price'] );
            $options[] = $option['option_name'];						
  				}
  			}
  
  			$is_trial_or_setup_fee = false;
  
  			if ( ! empty( $trial_field ) && $trial_field == $field_id ) {
  
  				$trial_amount = $product_price * $quantity;
  				$is_trial_or_setup_fee = true;
  
  			} elseif ( ! empty( $setup_fee_field ) && $setup_fee_field == $field_id ) {
  
  				$fee_amount = $product_price * $quantity;
  				$is_trial_or_setup_fee = true;
  			}
  
  			//Do not add to line items if the payment field selected in the feed is not the current field.
  			if ( is_numeric( $payment_field ) && $payment_field != $field_id ) {
  				continue;
  			}
  
  			//Do not add to line items if the payment field is set to "Form Total" and the current field was used for trial or setup fee.
  			if ( $is_trial_or_setup_fee && ! is_numeric( $payment_field ) ) {
  				continue;
  			}
  
  			$amount += $product_price * $quantity;
  
  			$description = '';
  			if ( ! empty( $options ) ) {
  				$description = esc_html__( 'options: ', 'gravityforms' ) . ' ' . implode( ', ', $options );
  			}
  
  			if ( $product_price >= 0 ) {
  				$line_items[] = array( 'id' => $field_id, 'name' => $product['name'], 'description' => $description, 'quantity' => $quantity, 'unit_price' => GFCommon::to_number( $product_price ), 'options' => rgar( $product, 'options' ) );
  			} else {
  				$discounts[] = array( 'id' => $field_id, 'name' => $product['name'], 'description' => $description, 'quantity' => $quantity, 'unit_price' => GFCommon::to_number( $product_price ), 'options' => rgar( $product, 'options' ) );
  			}
  		}
  
  		if ( $trial_field == 'enter_amount' ) {
              $trial_amount = rgar($feed["meta"], "trial_amount") ? GFCommon::to_number(rgar($feed["meta"], "trial_amount")) : 0;
  		}
  
  		if ( ! empty( $products['shipping']['name'] ) && ! is_numeric( $payment_field ) ) {    		
    		$line_items[] = array( 'id' => '', 'name' => $products['shipping']['name'], 'description' => '', 'quantity' => 1, 'unit_price' => GFCommon::to_number( MCrypt_Encryption::decrypt( $products['shipping']['price'] ) ), 'is_shipping' => 1 );
        $amount += MCrypt_Encryption::decrypt( $products['shipping']['price'] );		
  		}
  
  		return array( 'payment_amount' => $amount, 'setup_fee' => $fee_amount, 'trial' => $trial_amount, 'line_items' => $line_items, 'discounts' => $discounts );
  		
  	} else {
    	// No encryption, just act normal
      return parent::get_order_data( $feed, $form, $entry );
    }
  }
}
