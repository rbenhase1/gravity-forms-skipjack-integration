<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://ryanbenhase.com
 * @since             1.0.0
 * @package           Gravityforms_Skipjack
 *
 * @wordpress-plugin
 * Plugin Name:       Gravity Forms SKIPJACK Integration
 * Plugin URI:        https://bitbucket.org/rbenhase1/gravity-forms-skipjack-integration
 * Description:       This Plugin integrates Gravity Forms with SKIPJACK payment processing.
 * Version:           1.0.2
 * Author:            Ryan
 * Author URI:        http://ryanbenhase.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gravityforms-skipjack
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gravityforms-skipjack-activator.php
 */
function activate_gravityforms_skipjack() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravityforms-skipjack-activator.php';
	Gravityforms_Skipjack_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gravityforms-skipjack-deactivator.php
 */
function deactivate_gravityforms_skipjack() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravityforms-skipjack-deactivator.php';
	Gravityforms_Skipjack_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gravityforms_skipjack' );
register_deactivation_hook( __FILE__, 'deactivate_gravityforms_skipjack' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gravityforms-skipjack.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gravityforms_skipjack() {

	$plugin = new Gravityforms_Skipjack();
	$plugin->run();

}
add_action("plugins_loaded", 'run_gravityforms_skipjack' );
