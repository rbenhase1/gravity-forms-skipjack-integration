# Gravity Forms SKIPJACK Integration #
Contributors: rbenhase

Donate link: http://ryanbenhase.com

Tags: gravityforms

Requires at least: 4.2

Tested up to: 4.2

Stable tag: 4.2

License: GPLv2 or later

License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin enables a credit card field in the Gravity Forms plugin (which is required for this plugin to work); it can then be set up to work with the SKIPJACK payment processor by configuring one or more SKIPJACK feeds. 

## Description ##

For this plugin to be useful, you will need:

*   The [Gravity Forms](http://gravityforms.com/ "Gravity Forms") plugin installed and active
*   A [SKIPJACK](http://www.skipjack.com/default.aspx "SKIPJACK") account, including an HTML serial number for either the development (testing) or live environment
*   PHP/cURL
*   A little patience to ensure that things are working properly

This plugin supports simple, one-time credit card transactions, but it does not support shipping, taxation, subscriptions/recurring payments, etc. Sorry!

## Installation ##

This section describes how to install the plugin and get it working.

1. Upload `gravityforms-skipjack.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

## Usage ##

### Adding the credit card form fields ###

This plugin enables hidden credit card fields that come built-in to Gravity Forms. You will find them under the "Pricing Fields" section under the Form Editor. You'll want to use the "Credit Card" field to gather credit card data (which is NOT stored in the database, for security purposes), and the "Product" field to come up with the transaction total. 

### Configuring SKIPJACK integration ###

Use the SKIPJACK section under the form settings to add/edit a SKIPJACK feed (which is associated with your SKIPJACK account). Here, you can enter in your SKIPJACK HTML serial number. You will then need to select the appropriate environment (testing or live) and map your billing information form fields to the parameters which must be sent to SKIPJACK for payment processing. 

### Troubleshooting Errors ###

You can refer to the SKIPJACK Development Integration Guide to troubleshoot any errors and find out what each error code signifies.

## Changelog ##

* `1.01` Added support for data encrypted with the Simple MCrypt Encryption plugin.
* `1.02` Added CRT file to encourage cURL to trust remote CA
